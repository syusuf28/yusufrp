package com.example.retrofitdemo.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofitdemo.R
import com.example.retrofittest.model.Post
import kotlinx.android.synthetic.main.row_layout.view.*


class MyAdapter: RecyclerView.Adapter<MyAdapter.MyViewHolder>() {
    private var myList = emptyList<Post>()
    inner class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(
            R.layout.row_layout, parent, false
        ))
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.title_id.text = myList[position].title
        holder.itemView.body_id.text = myList[position].body

    }
    override fun getItemCount(): Int {
        return myList.size
    }

    fun setData(newList: Post){
        myList = newList
        notifyDataSetChanged()
    }
}